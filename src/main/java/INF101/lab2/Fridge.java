package INF101.lab2;

import java.util.ArrayList;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    
    int max_size;
    ArrayList<FridgeItem> food;

    // constructor
    public Fridge(){
        max_size = 20;
        food = new ArrayList<>();
    }

    
    public int totalSize() {
        return max_size;
    }
    
    @Override // greit å ha med siden da viser det at metoden kommer fra en annen klasse.
    public int nItemsInFridge() {
        return food.size();
        //return itemsInFridge;
    } 

    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() == max_size){
            return false;
        }
        food.add(item);
        return true;
    }

    public void takeOut(FridgeItem item){
        if (!(food.contains(item))) {
            throw new NoSuchElementException();
        } else {
            food.remove(item);
        }
    }

    public void emptyFridge() {
        food.clear();
    }

    public ArrayList<FridgeItem> removeExpiredFood() {
        // lager en ny ArrayList for utgåtte varer:
        ArrayList<FridgeItem> expiredFood = new ArrayList<>();
        // looper gjennom alle varene i kjøleskapet:
        for (int i=0; i < nItemsInFridge(); i++) {
            // for hver runde henter den et objekt fra food-arraylisten:
            // og sjekker om den har gått ut på dato:
            FridgeItem item = food.get(i);
            if (item.hasExpired()) {
                // dersom den har gått ut på dato, så legger den objektet til 
                // i den nye ArrayListen:
                expiredFood.add(item);
            }
        }
        // her looper den gjennom hvert element i expiredFood 
        // og kaller hvert element for expiredItem, slik som i=0 i en standard for-loop. 
        for (FridgeItem expiredItem : expiredFood) {
            food.remove(expiredItem);
        }
        return expiredFood;
    }
}
